<?php

namespace Drupal\acti_consultations\Controller;

use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class ExportController.
 */
class ExportController extends ControllerBase {

  /**
   * Export.
   *
   * @return string
   *   Return Hello string.
   */
  public function exportVote(int $consult_id) {

    $consult = Node::load($consult_id);
    $vote_type = $consult->get('field_acti_consult_type_vote')->value;

    $filename = file_directory_temp() . '/constulation-votes.csv';

    if (file_exists($filename)) {
      unlink($filename);
    }

    // Query.
    $database = \Drupal::database();
    $query = $database->select('proposition', 'p');
    $query->fields('pfapp', ['field_acti_proposition_prop_value']);

    $query->join('paragraph__field_acti_proposition_prop', 'pfapp', 'pfapp.entity_id=p.prop_id');

    if ($vote_type == 'etoiles') {
      $query->fields('p', ['moyenne', 'votes', 'etoiles']);
      $query->orderBy('p.moyenne', 'desc');
    }
    else {
      $query->fields('p', ['thumbs_up', 'thumbs_neutral', 'thumbs_down', 'votes']);
      $query->orderBy('p.votes', 'desc');
    }

    $query->fields('pfatt', ['field_acti_thematique_title_value']);
    $query->join('paragraph__field_acti_thematique_title', 'pfatt', 'pfatt.entity_id=p.thema_id');

    $query->condition('p.consult_id', $consult_id);

    $results = $query->execute()->fetchAll();

    // CSV Writing.
    $handle = fopen($filename, 'wb');
    // Handling Excel reading.
    fwrite($handle, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));

    if ($vote_type == 'etoiles') {
      fputcsv($handle, [
        'Proposition',
        'Moyenne de la proposition',
        'Nombre de votes',
        'Nombre total d\'étoiles',
        'Thématique',
      ], ';');
      foreach ($results as $key => $value) {
        fputcsv($handle, [
          $value->field_acti_proposition_prop_value,
          $value->moyenne, $value->votes,
          $value->etoiles,
          $value->field_acti_thematique_title_value,
        ], ';');
      }
    }
    else {
      fputcsv($handle, [
        'Proposition',
        'Pouces pour',
        'Pouces neutres',
        'Pouces contre',
        'Nombre de votes',
        'Thématique',
      ], ';');
      foreach ($results as $key => $value) {
        fputcsv($handle, [
          $value->field_acti_proposition_prop_value,
          $value->thumbs_up,
          $value->thumbs_neutral,
          $value->thumbs_down,
          $value->votes, $value->field_acti_thematique_title_value,
        ], ';');
      }
    }

    fclose($handle);

    return new BinaryFileResponse($filename, 200, [
      'Content-type' => 'text/csv',
      'Content-Disposition' => 'attachment; filename="constulation-votes.csv"',
    ]);
  }

  /**
   * Export.
   *
   * @return string
   *   Return Hello string.
   */
  public function exportComment(int $consult_id) {

    $filename = file_directory_temp() . '/constulation-comment.csv';

    if (file_exists($filename)) {
      unlink($filename);
    }

    // Query.
    $database = \Drupal::database();
    $query = $database->select('thematique_comment', 'tc');

    $query->fields('tc', ['created']);

    $query->fields('pfatt', ['field_acti_thematique_title_value']);
    $query->join('paragraph__field_acti_thematique_title', 'pfatt', 'pfatt.entity_id=tc.thematique_id');

    $query->fields('tc', ['comment']);

    $query->orderBy('pfatt.field_acti_thematique_title_value', 'asc');

    $query->condition('tc.consult_id', $consult_id);

    $results = $query->execute()->fetchAll();

    // CSV Writing.
    $handle = fopen($filename, 'wb');
    // Handling Excel reading.
    fwrite($handle, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));

    fputcsv($handle, [
      'Date',
      'Thématique',
      'Commentaire',
    ], ';');

    foreach ($results as $key => $value) {
      fputcsv($handle, [
        date('d/m/Y', $value->created),
        $value->field_acti_thematique_title_value,
        $value->comment,
      ], ';');
    }

    fclose($handle);

    return new BinaryFileResponse($filename, 200, [
      'Content-type' => 'text/csv',
      'Content-Disposition' => 'attachment; filename="constulation-comment.csv"',
    ]);
  }

}
