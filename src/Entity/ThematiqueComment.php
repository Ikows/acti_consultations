<?php

namespace Drupal\acti_consultations\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Thematique comment entity.
 *
 * @ingroup acti_consultations
 *
 * @ContentEntityType(
 *   id = "thematique_comment",
 *   label = @Translation("Thematique comment"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\acti_consultations\ThematiqueCommentListBuilder",
 *     "views_data" = "Drupal\acti_consultations\Entity\ThematiqueCommentViewsData",
 *
 *     "access" = "Drupal\acti_consultations\ThematiqueCommentAccessControlHandler",
 *   },
 *   base_table = "thematique_comment",
 *   translatable = FALSE,
 *   admin_permission = "administer thematique comment entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "consult_id" = "consult_id",
 *     "thematique_id" = "thematique_id",
 *     "comment" = "comment",
 *   },
 * )
 */
class ThematiqueComment extends ContentEntityBase implements ThematiqueCommentInterface {

  use EntityChangedTrait;

    /**
   * {@inheritdoc}
   */
  public function getconsultId() {
    return $this->get('consult_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setconsultId($consult_id) {
    $this->set('consult_id', $consult_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThematiqueId() {
    return $this->get('thematique_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setThematiqueId($thematique_id) {
    $this->set('thematique_id', $thematique_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getComment() {
    return $this->get('comment')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setComment($comment) {
    $this->set('comment', $comment);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['consult_id'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Consultation'))
    ->setDescription(t('The Consultation node.'))
    ->setSettings([
      'target_type' => 'node',
      'default_value' => 0,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(TRUE);

    $fields['thematique_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Thematique'))
      ->setDescription(t('The Thematique Comment entity.'))
      ->setSettings([
        'target_type' => 'paragraph',
        'default_value' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['comment'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Comment'))
      ->setDescription(t('The Comment of the Thematique entity.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
