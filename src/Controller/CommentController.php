<?php

namespace Drupal\acti_consultations\Controller;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\acti_consultations\Entity\ThematiqueComment;

/**
 * Class CommentController.
 */
class CommentController extends ControllerBase {

  /**
   * Handlecomment.
   *
   * @return string
   *   Return Hello string.
   */
  public function handleComment($themaid, Request $request) {
    if (!$thematique = Paragraph::load($themaid)) {
      $data['message'] = t('Thematique does not exist');
    }
    else {
      $comment = ThematiqueComment::create([
        'consult_id' => $thematique->get('parent_id')->value,
        'thematique_id' => $themaid,
        'comment' => htmlspecialchars($request->get('comment')),
      ]);
      if($comment->save()) {
        $data['message'] = t('OK');
      }
      else {
        $data['message'] = t('Error during Comment save');
      }
    }
    return new JsonResponse($data, 200);
  }

}
