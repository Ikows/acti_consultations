(function($, Drupal, drupalSettings) {

  /**
   * Preparation of page : cookie init and vote infos.
   */
  Drupal.behaviors.actiConsultationVoteAndCookie = {
    attach: function (context, settings) {
      let cookieData = {consult_id: drupalSettings.consult_id, userVote: {}};
      $.each(drupalSettings.votes_amounts, function (key, value) {
        // Preparing count results
        value = parseInt(value) + 1;
        let splitedStr = $('p[data-prop-id=' + key + ']').text().split('@count');
        $('p[data-prop-id=' + key + ']').once().html(splitedStr[0] + '<span>' + value + '</span>' + splitedStr[1]);
        // Preparing cookie data with propositions id
        cookieData.userVote[key] = 0
      })
      if (actiGetCookie('acti_consultations_' + drupalSettings.consult_id).length == 0) {
        actiSetCookie(cookieData)
      } else {
        // Set vote and disabling for every prop that has already been voted on.
        let cookie = JSON.parse(actiGetCookie('acti_consultations_' + drupalSettings.consult_id))
        for (var key in cookie.userVote) {
          if (cookie.userVote[key] != 0) {
            if (drupalSettings.consult_type == 'etoiles') {
              let starToTarget = $('div[data-prop-wrapper-id="' + key + '"]').find('a[data-mark="' + cookie.userVote[key] + '"]')
              disableVoteForProposition(starToTarget, 'remplie')
            } else {
              let thumbToTarget = $('div[data-prop-wrapper-id="' + key + '"]').find('a[data-mark="' + cookie.userVote[key] + '"]')
              thumbToTarget.addClass('actiVoted')
              console.log(thumbToTarget);
              
              thumbToTarget.parent().children().each(function () {
                $(this).removeClass('actiUnvoted')
              })
              // Showing vote count.
              $('#actiCountWrapper-' + thumbToTarget.parent().attr('data-prop-wrapper-id')).show()
            }
          }
        }
        // Switching buttons if thematique is filled.
        $('.actiToggled').each(function (key, value) {
          if ($(this).find('.actiUnvoted').length == 0) {
            $(this).parent().find('.actiOpen').hide()
            $(this).parent().find('.actiFilled').show()
          }
        })
      }
    }
  };

  /**
   * Ajax handling for vote.
   */
  Drupal.behaviors.actiConsultationAjaxVote = {
    attach: function (context, settings) {
      $('.acti-vote-link').once().click(function (e) {
        e.preventDefault()
        if ($(this).hasClass('actiUnvoted')) {
          $(this).addClass('actiVoted')
          let propId = $(this).parent().attr('data-prop-wrapper-id')
          $(this).parent().children().each(function () {
            $(this).toggleClass('actiUnvoted')
          })
          const aHref = $(this).attr('href')
          $.ajax({
            type: "GET",
            url: aHref,
            success: function () {
              $('#actiCountWrapper-' + propId).slideDown()
              // Storing data in cookie
              let cookie = JSON.parse(actiGetCookie('acti_consultations_' + drupalSettings.consult_id))
              let hrefArray = aHref.split('/')
              cookie.userVote[propId] = parseInt(hrefArray[hrefArray.length -1]);
              actiSetCookie(cookie)
            },
            error: function () {
              alert('Error, please try again later.')
            }
          })
        }
      })
    }
  };

  /**
   * Ajax handling for comments.
   */
  Drupal.behaviors.actiConsultationAjaxComment = {
    attach: function (context, settings) {
      $('.actiThemaSendComment').once().click(function (e) {
        const formSubmit = $(this);
        const actiForm = formSubmit.parent();
        const formAction = actiForm.attr('action');
        e.preventDefault()
        $.ajax({
          type: "POST",
          url: formAction,
          data: actiForm.serialize(),
          success: function () {
            formSubmit.toggleClass('bounceOutRight')
            
          },
          error: function () {
            alert('Error, please try again later.')
          }
        })
      })
    }
  };

  /**
   * Handle toggling.
   */
  Drupal.behaviors.actiConsultationToggle = {
    attach: function (context, settings) {
      $('.actiThematiqueToggler').once().click( function (e) {
        $(this).hide();
        $(this).parent().parent().find('.actiInstructions').show();
        $(this).parent().parent().parent().parent().find('.actiToggled').slideToggle()
      })

      $('.actiClose').once().click( function (e) {
        let toggled = $(this).parent().parent().parent();
        toggled.parent().find('.actiInstructions').hide();        
        toggled.slideToggle()
        if (toggled.find('.actiUnvoted').length == 0) {
          toggled.parent().find('.actiFilled').show();
        } else {
          toggled.parent().find('.actiOpen').show();
        }
      })
    }
  };

  Drupal.behaviors.actiConsultationToggleCroix = {
    attach: function (context, settings) {
      $('.actiCroix').once().click( function (e) {
        $(this).parent().slideToggle();
        $(this).parent().parent().find('.actiInstructions').hide();
        if ($(this).parent().find('.actiUnvoted').length == 0) {
          $(this).parent().parent().find('.actiFilled').show();
        } else {
          $(this).parent().parent().find('.actiOpen').show();
        }
      })
    }
  };


  /**
   * pouces et étoiles.
   */
  Drupal.behaviors.actiConsultationVotehover = {
    attach: function (context, settings) {
      $('.acti-etoiles').hover( function () {
        if ($(this).hasClass('actiUnvoted')) {
          etoileHov($(this), 'remplie')
        }
      }, function () {
        if ($(this).hasClass('actiUnvoted')) {
          etoileHov($(this), 'vide')
        }
      })
    }
  };

  /**
   * Set a cookie by with data.
   */
  function actiSetCookie(cookieData) {
    let dt = new Date();
    dt.setTime(dt.getTime() + 1000*3600*24*365)
    document.cookie = "acti_consultations_" + drupalSettings.consult_id + "=" + JSON.stringify(cookieData) + "; expires="+ dt.toUTCString() +"; path=/;"  
  }

  /**
   * Get a cookie by name.
   */
  function actiGetCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  /**
   * Recursive function ti change star image on hovering sibling.
   */
  function etoileHov(hovered, type) {
    hovered.find('img').attr('src', drupalSettings.acti_path + '/images/etoile-' + type + '.svg');
    if (hovered.prev().hasClass('acti-etoiles')) {
      hovered.prev().find('img').attr('src', drupalSettings.acti_path + '/images/etoile-' + type + '.svg');
      etoileHov(hovered.prev(), type)
    }
  }

  /**
   * Disable vote for a Proposition that has already been voted on.
   */
  function disableVoteForProposition(star, type) {
    etoileHov(star, type)
    star.parent().children().each(function () {
      $(this).removeClass('actiUnvoted')
    })
    // Showing vote count.
    $('#actiCountWrapper-' + star.parent().attr('data-prop-wrapper-id')).show()
  }

})(jQuery, Drupal, drupalSettings);