<?php

namespace Drupal\acti_consultations\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Thematique comment entities.
 *
 * @ingroup acti_consultations
 */
interface ThematiqueCommentInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Consultation parent of this Comment.
   *
   * @return string
   *   Id of the Thematique for this comment.
   */
  public function getConsultId();

  /**
   * Sets the Consultation parent of this Comment.
   *
   * @param string $consult_id
   *   The Consultation parent of this Comment.
   *
   * @return \Drupal\acti_consultations\Entity\ThematiqueCommentInterface
   *   The called Consultation parent of this Comment for Comment entity.
   */
  public function setConsultId($consult_id);

  /**
   * Gets the Thematique Id.
   *
   * @return string
   *   Id of the Thematique for this comment.
   */
  public function getThematiqueId();

  /**
   * Sets the Thematique Id.
   *
   * @param string $thematique_id
   *   The Thematique Id.
   *
   * @return \Drupal\acti_consultations\Entity\ThematiqueCommentInterface
   *   The called Thematique id for Comment entity.
   */
  public function setThematiqueId($thematique_id);

    /**
   * Gets the Comment.
   *
   * @return string
   *   The Comment value for this comment.
   */
  public function getComment();

  /**
   * Sets the Comment.
   *
   * @param string $comment
   *   The Comment.
   *
   * @return \Drupal\acti_consultations\Entity\ThematiqueCommentInterface
   *   The called Comment for Comment entity.
   */
  public function setComment($comment);

  /**
   * Gets the Thematique comment creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Thematique comment.
   */
  public function getCreatedTime();

  /**
   * Sets the Thematique comment creation timestamp.
   *
   * @param int $timestamp
   *   The Thematique comment creation timestamp.
   *
   * @return \Drupal\acti_consultations\Entity\ThematiqueCommentInterface
   *   The called Thematique comment entity.
   */
  public function setCreatedTime($timestamp);

}
