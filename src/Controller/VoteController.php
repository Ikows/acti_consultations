<?php

namespace Drupal\acti_consultations\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\acti_consultations\Entity\Proposition;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Vote managing.
 */
class VoteController extends ControllerBase {

  /**
   * Get the votes per Proposition.
   *
   * @param integer $propid
   *  Id of the proposition.
   * @param integer $mark
   *  Mark for the vote.
   * @return JsonResponse
   *  Return OK or KO response.
   */
  public function vote(int $propid, int $mark): JsonResponse {
    if (!in_array($mark, [1,2,3,4,5])) {
      $data['message'] = t('Forbidden mark value');
    }
    else {
      $themaQuery = \Drupal::entityQuery('proposition')
        ->condition('prop_id', $propid)
        ->execute();
      if (!$proposition = Proposition::load(reset($themaQuery))) {
        $data['message'] = t('Proposition does not exist');
      }
      else {
        $proposition
          ->setEtoiles($proposition->getEtoiles() + $mark)
          ->setVotes($proposition->getVotes() + 1)
          ->setMoyenne($proposition->getEtoiles() / $proposition->getVotes());
        if($proposition->save()) {
          $data['message'] = t('OK');
          Cache::invalidateTags(['node:' . $proposition->getConsultId()]);
        }
        else {
          $data['message'] = t('Error during vote save');
        }
      }
    }
    return new JsonResponse($data, 200);
  }

    /**
   * Get the thumbs votes per Proposition.
   *
   * @param integer $propid
   *  Id of the proposition.
   * @param integer $mark
   *  Mark for the vote.
   * @return JsonResponse
   *  Return OK or KO response.
   */
  public function voteThumbs(int $propid, int $mark): JsonResponse {
    if (!in_array($mark, [1,2,3])) {
      $data['message'] = t('Forbidden mark value');
    }
    else {
      $themaQuery = \Drupal::entityQuery('proposition')
        ->condition('prop_id', $propid)
        ->execute();
      if (!$proposition = Proposition::load(reset($themaQuery))) {
        $data['message'] = t('Proposition does not exist');
      }
      else {
        switch ($mark) {
          case 1:
            $proposition->setThumbsUp($proposition->getThumbsUp() + 1);
            break;
          case 2:
            $proposition->setThumbsNeutral($proposition->getThumbsNeutral() + 1);
            break;
          case 3:
            $proposition->setThumbsDown($proposition->getThumbsDown() + 1);
            break;
          default:
            break;
        }
        $proposition->setVotes($proposition->getVotes() + 1);
        if($proposition->save()) {
          $data['message'] = t('OK');
          Cache::invalidateTags(['node:' . $proposition->getConsultId()]);
        }
        else {
          $data['message'] = t('Error during vote save');
        }
      }
    }
    return new JsonResponse($data, 200);
  }
}

//TODO vote pour pouces + tester xhr pour eviter mass vote.