<?php

namespace Drupal\acti_consultations;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Thematique comment entity.
 *
 * @see \Drupal\acti_consultations\Entity\ThematiqueComment.
 */
class ThematiqueCommentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\acti_consultations\Entity\ThematiqueCommentInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished thematique comment entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published thematique comment entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit thematique comment entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete thematique comment entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add thematique comment entities');
  }


}
