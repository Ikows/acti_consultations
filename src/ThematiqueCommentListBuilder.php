<?php

namespace Drupal\acti_consultations;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Thematique comment entities.
 *
 * @ingroup acti_consultations
 */
class ThematiqueCommentListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Thematique comment ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\acti_consultations\Entity\ThematiqueComment $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.thematique_comment.edit_form',
      ['thematique_comment' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
