<?php

namespace Drupal\acti_consultations;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Proposition entity.
 *
 * @see \Drupal\acti_consultations\Entity\Proposition.
 */
class PropositionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\acti_consultations\Entity\PropositionInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished proposition entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published proposition entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit proposition entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete proposition entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add proposition entities');
  }

}
