<?php

namespace Drupal\acti_consultations\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Proposition entity.
 *
 * @ingroup acti_consultations
 *
 * @ContentEntityType(
 *   id = "proposition",
 *   label = @Translation("Proposition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\acti_consultations\PropositionListBuilder",
 *     "views_data" = "Drupal\acti_consultations\Entity\PropositionViewsData",
 *
 *     "access" = "Drupal\acti_consultations\PropositionAccessControlHandler",
 *   },
 *   base_table = "proposition",
 *   admin_permission = "administer proposition entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "prop_id" = "prop_id",
 *     "thema_id" = "thema_id",
 *     "consult_id" = "consult_id",
 *     "votes" = "votes",
 *     "etoiles" = "etoiles",
 *     "thumbs_up" = "thumbs_up",
 *     "thumbs_neutral" = "thumbs_neutral",
 *     "thumbs_down" = "thumbs_down",
 *     "moyenne" = "moyenne",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 * )
 */
class Proposition extends ContentEntityBase implements PropositionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getPropId() {
    return $this->get('prop_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPropId($prop_id) {
    $this->set('prop_id', $prop_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThemaId() {
    return $this->get('thema_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setThemaId($thema_id) {
    $this->set('thema_id', $thema_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsultId() {
    return $this->get('consult_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setConsultId($consult_id) {
    $this->set('consult_id', $consult_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVotes() {
    return $this->get('votes')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVotes($votes) {
    $this->set('votes', $votes);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEtoiles() {
    return $this->get('etoiles')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEtoiles($etoiles) {
    $this->set('etoiles', $etoiles);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbsUp() {
    return $this->get('thumbs_up')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setThumbsUp($thumbs_up) {
    $this->set('thumbs_up', $thumbs_up);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbsNeutral() {
    return $this->get('thumbs_neutral')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setThumbsNeutral($thumbs_neutral) {
    $this->set('thumbs_neutral', $thumbs_neutral);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbsDown() {
    return $this->get('thumbs_down')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setThumbsDown($thumbs_down) {
    $this->set('thumbs_down', $thumbs_down);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMoyenne() {
    return $this->get('moyenne')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMoyenne($moyenne) {
    $this->set('moyenne', $moyenne);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['prop_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Proposition'))
      ->setDescription(t('The prop_id of the Proposition entity.'))
      ->setSettings([
        'target_type' => 'paragraph',
        'default_value' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['thema_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Thématique'))
      ->setDescription(t('The thema_id of the Proposition entity.'))
      ->setSettings([
        'target_type' => 'paragraph',
        'default_value' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['consult_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Consultation'))
      ->setDescription(t('The consult_id of the Proposition entity.'))
      ->setSettings([
        'target_type' => 'node',
        'default_value' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['votes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Votes de la proposition'))
      ->setDescription(t('The votes of the Proposition entity.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE);

    $fields['etoiles'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Etoiles de la proposition'))
      ->setDescription(t('The etoiles of the Proposition entity.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE);

    $fields['thumbs_up'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Pouces en haut de la proposition'))
      ->setDescription(t('The thumbs up of the Proposition entity.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE);

    $fields['thumbs_neutral'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Pouces neutres de la proposition'))
      ->setDescription(t('The thumbs neutral of the Proposition entity.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE);

    $fields['thumbs_down'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Pouces en bas de la proposition'))
      ->setDescription(t('The thumbs down of the Proposition entity.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE);


    $fields['moyenne'] = BaseFieldDefinition::create('float')
    ->setLabel(t('Moyenne de la proposition'))
    ->setDescription(t('The moyenne of the Proposition entity.'))
    ->setDefaultValue(0)
    ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Proposition is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
