<?php

namespace Drupal\acti_consultations\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Proposition entities.
 *
 * @ingroup acti_consultations
 */
interface PropositionInterface extends ContentEntityInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Proposition prop_id.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getPropId();

  /**
   * Sets the Proposition prop_id.
   *
   * @param string $prop_id
   *   The Proposition prop_id.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setPropId($prop_id);

  /**
   * Gets the Proposition thema_Id.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getThemaId();

  /**
   * Sets the Proposition thema_Id.
   *
   * @param string $thema_Id
   *   The Proposition thema_Id.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setThemaId($thema_Id);

  /**
   * Gets the Proposition consult_id.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getConsultId();

  /**
   * Sets the Proposition consult_id.
   *
   * @param string $consult_id
   *   The Proposition consult_id.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setConsultId($consult_id);

  /**
   * Gets the Proposition votes.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getVotes();

  /**
   * Sets the Proposition votes.
   *
   * @param string $votes
   *   The Proposition votes.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setVotes($votes);

  /**
   * Gets the Proposition etoiles.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getEtoiles();

  /**
   * Sets the Proposition etoiles.
   *
   * @param string $etoiles
   *   The Proposition etoiles.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setEtoiles($etoiles);

  /**
   * Gets the Proposition thumbs_up.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getThumbsUp();

  /**
   * Sets the Proposition thumbs_up.
   *
   * @param string $thumbs_up
   *   The Proposition thumbs_up.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setThumbsUp($thumbs_up);

  /**
   * Gets the Proposition thumbs_neutral.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getThumbsNeutral();

  /**
   * Sets the Proposition thumbs_neutral.
   *
   * @param string $thumbs_neutral
   *   The Proposition thumbs_neutral.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setThumbsNeutral($thumbs_neutral);

    /**
   * Gets the Proposition thumbs_down.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getThumbsDown();

  /**
   * Sets the Proposition thumbs_down.
   *
   * @param string $thumbs_down
   *   The Proposition thumbs_down.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setThumbsDown($thumbs_down);

  /**
   * Gets the Proposition moyenne.
   *
   * @return string
   *   Contenu of the Proposition.
   */
  public function getMoyenne();

  /**
   * Sets the Proposition moyenne.
   *
   * @param string $moyenne
   *   The Proposition moyenne.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setMoyenne($moyenne);

  /**
   * Gets the Proposition creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Proposition.
   */
  public function getCreatedTime();

  /**
   * Sets the Proposition creation timestamp.
   *
   * @param int $timestamp
   *   The Proposition creation timestamp.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Proposition published status indicator.
   *
   * Unpublished Proposition are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Proposition is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Proposition.
   *
   * @param bool $published
   *   TRUE to set this Proposition to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\acti_consultations\Entity\PropositionInterface
   *   The called Proposition entity.
   */
  public function setPublished($published);

}
