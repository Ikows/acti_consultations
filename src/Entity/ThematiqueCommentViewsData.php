<?php

namespace Drupal\acti_consultations\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Thematique comment entities.
 */
class ThematiqueCommentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
