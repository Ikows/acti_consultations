(function($, Drupal, drupalSettings) {

  /**
   * Preparation of page : cookie init and vote infos.
   */
  Drupal.behaviors.actiConsultationSideCustom = {
    attach: function (context, settings) {
      let SideCustomFieldsArray = [
        'edit-field-acti-consult-thema-close-wrapper',
        'edit-field-acti-consult-thema-open-wrapper',
        'edit-field-acti-consult-instruction-wrapper',
        'edit-field-acti-consult-button-color-wrapper',
        'edit-field-acti-consult-btn-txt-color-wrapper',
        'edit-field-acti-consult-thema-filled-wrapper',
        'edit-field-acti-consult-filled-color-wrapper',
        'edit-field-acti-consult-fild-tx-color-wrapper',
        'edit-field-acti-consult-vote-amount-wrapper',
        'edit-field-acti-consult-comment-txt-wrapper',
        'edit-field-acti-consult-prop-icon-wrapper'
      ]
      let sideDiv = $('<div class="advancedWrapper"></div>')
      $('.layout-region.layout-region-node-secondary', context).once('layout-region-node-secondary').append(sideDiv);
      let titleWrapper = '<div class="entity-meta js-form-wrapper form-wrapper">'
      + '<div class="entity-meta__header js-form-wrapper form-wrapper">'
      + '<div class="entity-meta__title js-form-item form-item js-form-type-item form-type-item form-no-label">Personalisation Avancée</div>'
      + '</div>'
      + '</div>'

      $('.advancedWrapper', context).once('advancedWrapper').append(titleWrapper)
      $(SideCustomFieldsArray).each(function (key, value) {
        $('.advancedWrapper').find('.entity-meta__header').append($('#' + value))
      })
    }
  };

})(jQuery, Drupal, drupalSettings);